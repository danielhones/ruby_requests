[![pipeline status](https://gitlab.com/danielhones/ruby_requests/badges/master/pipeline.svg)](https://gitlab.com/danielhones/ruby_requests/commits/master)
[![coverage report](https://gitlab.com/danielhones/ruby_requests/badges/master/coverage.svg)](https://gitlab.com/danielhones/ruby_requests/commits/master)


# [Ruby Requests](https://rubygems.org/gems/ruby-requests)

For Python programmers who just can't avoid making HTTP requests in Ruby.

This gem is a Ruby version of Kenneth Reitz's excellent [Requests](https://github.com/requests/requests) library for Python.  The immediate goal for this project is to provide a useful subset of the interface and features supported by the Python library.  It supports MRI Ruby 2.3.0 and up.  A lot of the core functionality has been implemented already but it is still under development.

# Installation

```
gem install ruby-requests --pre
```

Then `require 'requests'` in your Ruby code to use it.


# Things that work now

The following methods are implemented:

* `Requests.get`
* `Requests.post`
* `Requests.put`
* `Requests.head`
* `Requests.delete`
* `Requests.patch`
* `Requests.options`

Because of Ruby's handling of keyword arguments, optional parameters must be passed by keyword.  For basic usage, here are some examples:

GET request with no query string params, parse the JSON response:

```
> require 'requests'
true
> r = Requests.get('https://httpbin.org/anything')
 => #<Requests::Response 200 OK>
> r.status_code
 => 200
> r.reason
 => "OK"
# .text returns the body of the response as text:
> puts r.text
{
  "args": {},
  "data": "",
  "files": {},
  "form": {},
  "headers": {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Connection": "close",
    "Host": "httpbin.org",
    "User-Agent": "ruby-requests/0.0.1.a1"
  },
  "json": null,
  "method": "GET",
  "url": "https://httpbin.org/anything"
}
# the .json method parses the response body as JSON, returning a Ruby object:
> pp r.json
{"args"=>{},
 "data"=>"",
 "files"=>{},
 "form"=>{},
 "headers"=>
  {"Accept"=>"*/*",
   "Accept-Encoding"=>"gzip, deflate",
   "Connection"=>"close",
   "Host"=>"httpbin.org",
   "User-Agent"=>"ruby-requests/0.0.1.a1"},
 "json"=>nil,
 "method"=>"GET",
 "url"=>"https://httpbin.org/anything"}
```

POST request with query string params and text body:

```
> r = Requests.post('https://httpbin.org/anything',
*                   params: {'testkey' => 'test value', 'otherkey' => 'other value'},
*                   data: "sample POST body")
> r.url
 => "https://httpbin.org/anything?testkey=test+value&otherkey=other+value"
> r.json['args']
 => {"otherkey"=>"other value", "testkey"=>"test value"}
> r.json['data']
 => "sample POST body"
```

Add headers to a request;

```
> r = Requests.get('https://httpbin.org/headers', headers: {'Test-Header' => 'Example'})
 => #<Requests::Response 200 OK> 
> puts r.text
{
  "headers": {
    "Accept": "*/*", 
    "Accept-Encoding": "gzip, deflate", 
    "Connection": "close", 
    "Host": "httpbin.org", 
    "Test-Header": "Example", 
    "User-Agent": "ruby-requests/0.0.1.a1"
  }
}
```


Skip verification of SSL certificate (don't do this):

```
# Error by default on a self-signed cert:
> r = Requests.get('https://self-signed.badssl.com')
Traceback (most recent call last):
   ...
OpenSSL::SSL::SSLError (SSL_connect returned=1 errno=0 state=error: certificate verify failed (self signed certificate))

# Pass verify: false to prevent verification:
> r = Requests.get('https://self-signed.badssl.com', verify: false)
 => #<Requests::Response 200 OK>
```

Use Basic Authentication (currently the only supported auth type):

```
> r = Requests.get('https://httpbin.org/basic-auth/user/password',
*                  auth: ['user', 'password'])
 => #<Requests::Response 200 OK>
```

Run through a proxy:

```
> r = Requests.get('http://example.com', proxies: {'http' => 'http://172.20.9.13:3128'})
 => #<Requests::Response 200 OK>
```


# Sessions

Sessions are partially supported in the same way that they work in the Python version.  Here's a list of things that work:

* Cookie persistence across requests.  `Session#cookies` returns a `Requests::CookieJar` object which is a subclass of [HTTP::Cookie](https://github.com/sparklemotion/http-cookie).  See documentation for that gem for its main functionality.
* Optional authentication sent with each request
* Session headers sent with each request (per-request headers take precedence if used)
* Proxies used for each request in a session

And some things that don't work yet:

* Respecting environment variables such as `http_proxy`
* Following redirects


# Features that haven't been added yet

* **Redirects:** Following redirects is one of the next things to be implemented but they currently are not followed.  There is an `allow_redirects` keyword argument on the request methods that defaults to true, but at the moment it is just ignored.
* **Hooks:** There is currently no implementation of hooks or block passing to the request methods
