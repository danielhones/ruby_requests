require 'simplecov'

SimpleCov.start { add_filter 'spec/' } unless ENV['COVERAGE'] == 'false'

require_relative '../lib/requests'

REQUESTS_VERBOSE = (ENV['REQUESTS_VERBOSE'] == 'true')
ENV['REQUESTS_HTTP_DEBUG'] = 'true' if REQUESTS_VERBOSE

HTTPBIN_URLS = {
    'HTTP' => 'http://httpbin.org',
    'HTTPS' => 'https://httpbin.org',
}

BADSSL_URLS = {
    'self-signed' => 'https://self-signed.badssl.com/',
}

$log = StringIO.new
$logger = Requests::Logger.new($log, level: 'debug', enabled: true)
Requests.logger = $logger


def reset_log
    before(:each) do
        $log.seek(0)
    end
end

def display_verbose_log
    after(:each) do
        if REQUESTS_VERBOSE
            $log.seek(0)
            puts "\nCAPTURED LOG:"
            puts "=" * 16
            puts $log.read
            puts "=" * 16 + "\n"
        end
    end
end

def running_in_docker
    ENV['RUNNING_IN_DOCKER'] == 'true'
end
