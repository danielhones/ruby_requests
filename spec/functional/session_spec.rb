require 'rspec'
require_relative '../spec_helper'


RSpec.describe Requests::Session do
    let(:httpbin) {
        HTTPBIN_URLS['HTTPS']
    }

    let(:anything_url) {
        httpbin + '/anything'
    }

    before(:each) do
        @session = Requests::Session.new
    end

    describe "headers" do
        before(:each) do
            @session_hdrs = {
                'Test-Header' => 'test value',
                'Other-Header' => 'other value',
            }
            @session.headers = @session_hdrs
        end

        it "sends session headers with each request" do
            r = @session.get(anything_url)
            expect(r.json['headers']['Test-Header']).to(
                eq(@session_hdrs['Test-Header'])
            )
            expect(r.json['headers']['Other-Header']).to(
                eq(@session_hdrs['Other-Header'])
            )
        end

        it "request headers take precedence over session headers" do
            req_hdrs = {'Test-Header' => 'request-specific'}
            r = @session.get(anything_url, headers: req_hdrs)
            expect(r.json['headers']['Test-Header']).to(
                eq(req_hdrs['Test-Header'])
            )
            expect(r.json['headers']['Other-Header']).to(
                eq(@session_hdrs['Other-Header'])
            )
        end

        it "session headers can be updated" do
            @session.headers['New-Header'] = 'new value'
            r = @session.put(anything_url)
            expect(r.json['headers']['New-Header']).to eq('new value')
        end
    end

    describe "cookies" do
        let(:cookie_url) { httpbin + '/cookies' }

        before(:each) do
            @cookies = Requests::CookieJar.new
            @cookies.add(
                Requests::Cookie.new('chocolate', 'chip', domain: 'httpbin.org',
                                     for_domain: true, path: '/')
            )
            @cookies.add(
                Requests::Cookie.new('or', 'eo', domain: 'httpbin.org',
                                     for_domain: true, path: '/')
            )
            @session.cookies = @cookies
        end

        it "sends session cookies with each request" do
            r = @session.get(cookie_url)
            expect(r.json['cookies']).to(
                eq({'chocolate' => 'chip', 'or' => 'eo'})
            )
        end

        it "request cookies take precedence over session cookies" do
            r = @session.get(cookie_url, cookies: {'or' => 'something else'})
            expect(r.json['cookies']).to(
                eq({'chocolate' => 'chip',
                    'or' => 'something else'})
            )
        end

        it "only sends cookies belonging to request uri" do
            # this will set some cookies for github.com:
            r = @session.get("https://github.com")
            github_cookies = r.cookies.to_hash

            r = @session.get(cookie_url)
            github_cookies.each_key do |key|
                expect(r.json['cookies']).not_to include(key)
            end
        end

        it "persists cookies set by requests" do
            req_cookies = {'new-cookie' => 'new value',
                                      'other-cookie' => 'other value'}
            @session.get(cookie_url + '/set', params: req_cookies)
            r = @session.get(cookie_url)
            expect(r.json['cookies']).to(
                eq({'chocolate' => 'chip',
                    'or' => 'eo'}.merge(req_cookies))
            )
        end
    end

    # TODO: Write tests for proxies in a Session after figuring out
    #       a good automated way to test proxies
end
