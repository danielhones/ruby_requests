
require 'rspec'
require 'base64'
require 'openssl'
require_relative '../spec_helper'


RSpec.describe Requests do
    reset_log
    display_verbose_log

    shared_examples "bad SSL certificates" do |method|
        it "raises exception on self-signed cert" do
            url = BADSSL_URLS['self-signed']
            expect {
                Requests.send(method, url)
            }.to raise_error(OpenSSL::SSL::SSLError)
        end

        it "doesn't raise exception when verify: false" do
            url = BADSSL_URLS['self-signed']
            expect {
                Requests.send(method, url, verify: false)
            }.not_to raise_error
        end
    end

    shared_examples "redirects" do |method, protocol, httpbin|
        it "#{protocol} follows redirect with allow_redirects: true" do
            skip "Not implemented yet"
            url = httpbin + '/redirect-to'
            payload = {'url' => 'http://example.com'}
            resp = Requests.send(method, url, params: payload,
                                 allow_redirects: true)
            expect(resp.status_code).to eq(200)
            expect(resp.url).to eq('http://example.com')
        end

        it "#{protocol} doesn't follow redirect with allow_redirects: false" do
            url = httpbin + '/redirect-to'
            payload = {'url' => 'http://example.com'}
            resp = Requests.send(method, url, params: payload,
                                 allow_redirects: false)
            expect(resp.status_code).to eq(302)
            full_url = url + '?' + URI.encode_www_form(payload)
            expect(resp.url).to eq(full_url)
        end
    end

    shared_examples "all methods" do |method, protocol, httpbin|
        it "#{protocol} sets url attribute on the response" do
            url = httpbin + '/anything'
            resp = Requests.send(method, url)
            expect(resp.url).to eq(url)
        end

        it "uses timeout" do
            url = httpbin + '/anything'
            expect {
                Requests.send(method, url, timeout: [0.00001, 2])
            }.to raise_error(Requests::ConnectionTimeout)
        end
    end

    shared_examples "methods with response body" do |method, protocol, httpbin|
        let(:url) { httpbin + '/anything' }

        it "#{protocol} makes request" do
            resp = Requests.send(method, url)
            received_headers = Requests::Utils::InsensitiveDict
                               .new(resp.json['headers'])
            expect(resp.json['args']).to eq({})
            user_agent = received_headers['User-Agent']
            expect(user_agent).to eq(Requests::Utils.default_user_agent)
        end

        it "#{protocol} sends query string params" do
            payload = {
                'example_key' => 'example_value',
                'other_key' => 'other_value',
                'another_key' => ['this', 'is', 'an', 'array'],
            }
            resp = Requests.send(method, url, params: payload)
            expect(resp.json['args']).to eq(payload)
        end

        it "#{protocol} sends headers" do
            headers = {
                'Header-Name' => 'header value',
                'Other-Header' => 'other value',
                'lowercase-header' => 'should still match',
                'User-Agent' => 'Mozilla whatever',
            }
            resp = Requests.send(method, url, headers: headers)
            sent_headers = Requests::Utils::InsensitiveDict
                           .new(resp.json['headers'])
            headers.each do |k, v|
                expect(sent_headers[k]).to eq(v)
            end
        end

        it "#{protocol} sends cookies" do
            cookies = {
                'chocolate' => 'chip',
                'oatmeal' => 'raisin hell',
            }
            resp = Requests.send(method, url, cookies: cookies)
            sent_cookies = resp.json['headers']['Cookie']
            expect(sent_cookies).to include('chocolate=chip')
            expect(sent_cookies).to include('oatmeal="raisin hell"')
        end
    end

    shared_examples "GET method" do |protocol, httpbin|
        it "#{protocol} sends basic auth" do
            url = httpbin + '/basic-auth/testuser/testpassword'
            resp = Requests.get(url, auth: ['testuser', 'testpassword'])
            expect(resp.status_code).to eq(200)
        end

        it "uses timeout" do
            url = httpbin + '/delay/60'
            expect {
                Requests.get(url, timeout: 2)
            }.to raise_error(Requests::TimeoutError)
        end

        it "#{protocol} decodes gzip" do
            url = httpbin + '/gzip'
            resp = Requests.get(url, headers: {'Test-Header' => 'value'})
            expect(resp.json['headers']['Test-Header']).to eq('value')
        end

        it "#{protocol} decodes deflate" do
            url = httpbin + '/deflate'
            resp = Requests.get(url, headers: {'Test-Header' => 'value'})
            expect(resp.json['headers']['Test-Header']).to eq('value')
        end

        it "#{protocol} sets response cookies from Set-Cookie header" do
            url = httpbin + '/cookies/set'
            set_cookies = {
                'macadamia' => 'nut',
                'peanut' => 'butter',
            }
            resp = Requests.get(url, params: set_cookies,
                                allow_redirects: false)
            expect(resp.cookies_hash).to eq(set_cookies)
        end

        it "#{protocol} handles unicode" do
            url = httpbin + '/encoding/utf8'
            unicode_str = "Зарегистрируйтесь сейчас на"
            r = Requests.get(url)
            expect(r.text).to include(unicode_str)
        end
    end

    shared_examples "methods with request body" do |method, protocol, httpbin|
        let(:url) { httpbin + '/anything' }

        it "#{protocol} sends json object as body" do
            json_payload = {
                'some' => 'thing',
                'that' => ['can', 'be', 'encoded'],
                'as' => {'json' => true},
            }
            resp = Requests.send(method, url, json: json_payload)
            expect(resp.json['json']).to eq(json_payload)
        end

        it "#{protocol} sends json array as body" do
            json_payload = ['some', 'list', 'of', 7, {'items' => [1, 42]}]
            resp = Requests.send(method, url, json: json_payload)
            expect(resp.json['json']).to eq(json_payload)
        end

        it "#{protocol} sends data string as body" do
            data = "Some sample data\nthat isn't JSON\n"
            resp = Requests.send(method, url, data: data)
            expect(resp.json['data']).to eq(data)
        end

        it "#{protocol} sends data hash as form-urlencoded" do
            data = {
                "key a" => "value a",
                "key b" => "value b",
            }
            resp = Requests.send(method, url, data: data)
            expect(resp.json['form']).to eq(data)
        end

        it "#{protocol} sends data array as form-urlencoded" do
            data = [["key a", "value a"],
                    ["key b", "value b"]]
            expected = {
                "key a" => "value a",
                "key b" => "value b",
            }
            resp = Requests.send(method, url, data: data)
            expect(resp.json['form']).to eq(expected)
        end

        it "#{protocol} sends files" do
        end

        it "#{protocol} sends basic auth" do
            url = httpbin + '/basic-auth/testuser/testpassword'
            resp = Requests.send(method, url, auth: ['testuser',
                                                     'testpassword'])

            # Can't check response status code here because httpbin.org
            # only allows GET requests on /basic-auth:
            auth_token = Base64.strict_encode64('testuser:testpassword')
            encoded_auth = "Basic #{auth_token}"
            auth_header = resp.request.headers['Authorization']
            expect(auth_header).to eq(encoded_auth)
        end
    end

    shared_examples "HEAD method" do |protocol, httpbin|
        let(:url) { httpbin + '/anything' }

        it "#{protocol} makes request" do
            resp = Requests.head(url)
            expect(resp.status_code).to eq(200)
        end

        it "#{protocol} body is empty" do
            resp = Requests.head(url)
            expect(resp.text).to eq('')
        end
    end

    shared_examples "OPTIONS method" do |protocol, httpbin|
        let(:all_methods) {
            ['PUT', 'POST', 'OPTIONS', 'DELETE',
             'HEAD', 'GET', 'TRACE', 'PATCH']
        }

        it "#{protocol} gets allow header in response /anything" do
            url = httpbin + "/anything"
            resp = Requests.options(url)
            allowed = resp.headers['allow'].split(', ')
            diff = (allowed - all_methods) + (all_methods - allowed)
            expect(diff).to eq([])
        end

        it "#{protocol} gets allow header in response /delete" do
            url = httpbin + "/delete"
            resp = Requests.options(url)
            allowed = resp.headers['allow'].split(', ')
            methods = ['OPTIONS', 'DELETE']
            diff = (allowed - methods) + (methods - allowed)
            expect(diff).to eq([])
        end
    end

    describe "GET" do
        HTTPBIN_URLS.each do |protocol, httpbin|
            include_examples("all methods", "get", protocol, httpbin)
            include_examples("redirects", "get", protocol, httpbin)
            include_examples("methods with response body", "get", protocol,
                             httpbin)
            include_examples("GET method", protocol, httpbin)
        end
        include_examples("bad SSL certificates", 'get')
    end

    describe "POST" do
        HTTPBIN_URLS.each do |protocol, httpbin|
            include_examples("all methods", "post", protocol, httpbin)
            include_examples("redirects", "post", protocol, httpbin)
            include_examples("methods with response body", "post", protocol,
                             httpbin)
            include_examples("methods with request body", "post", protocol,
                             httpbin)
        end
        include_examples("bad SSL certificates", 'post')
    end

    describe "PUT" do
        HTTPBIN_URLS.each do |protocol, httpbin|
            include_examples("all methods", "put", protocol, httpbin)
            include_examples("redirects", "put", protocol, httpbin)
            include_examples("methods with response body", "put", protocol,
                             httpbin)
            include_examples("methods with request body", "put", protocol,
                             httpbin)
        end
    end

    describe "PATCH" do
        HTTPBIN_URLS.each do |protocol, httpbin|
            include_examples("all methods", "patch", protocol, httpbin)
            include_examples("redirects", "patch", protocol, httpbin)
            include_examples("methods with response body", "patch", protocol,
                             httpbin)
            include_examples("methods with request body", "patch", protocol,
                             httpbin)
        end
    end

    describe "HEAD" do
        HTTPBIN_URLS.each do |protocol, httpbin|
            include_examples("all methods", "head", protocol, httpbin)
            include_examples("redirects", "head", protocol, httpbin)
            include_examples("HEAD method", protocol, httpbin)
        end
    end

    describe "DELETE" do
        HTTPBIN_URLS.each do |protocol, httpbin|
            include_examples("all methods", "delete", protocol, httpbin)
            include_examples("redirects", "delete", protocol, httpbin)
            # include_examples("DELETE method", protocol, httpbin)
        end
    end

    describe "OPTIONS" do
        HTTPBIN_URLS.each do |protocol, httpbin|
            include_examples("all methods", "options", protocol, httpbin)
            include_examples("OPTIONS method", protocol, httpbin)
        end
    end
end
