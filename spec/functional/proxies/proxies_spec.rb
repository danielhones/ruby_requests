require_relative '../../spec_helper'

##
# These tests will run from inside the docker container named
# "ruby", set up by the docker-compose called in spec/docker_tests.sh

RSpec.describe 'proxies', :if => running_in_docker do
    Requests.enable_dev_mode

    let(:proxy) { ENV['PROXY_HOST'] + ":" + ENV['PROXY_PORT'] }

    # These are hard-coded in the squid_conf/passwords file which gets
    # placed on the squid proxy container:
    let(:proxy_user) { "foobar" }
    let(:proxy_pass) { "SecretPass" }

    before(:each) do
        # The squid_logs directory is shared in a volume with both
        # the proxy and ruby docker containers:
        @proxy_log_file = File.join(__dir__, 'squid_logs/access.log')
        if File.exist?(@proxy_log_file)
            @proxy_log = File.open(@proxy_log_file)
            @proxy_log.read
        end
        proxy_creds = "http://#{proxy_user}:#{proxy_pass}@#{proxy}"
        proxy_no_creds = "http://#{proxy}"
        @proxies = {'http' => proxy_creds, 'https' => proxy_creds}
        @proxies_no_creds = {'http' => proxy_no_creds,
                             'https' => proxy_no_creds}
    end

    after(:each) do
        @proxy_log.close if @proxy_log
    end

    context "HTTP" do
        before(:each) do
            @url = HTTPBIN_URLS['HTTP'] + '/anything'
        end

        it 'through proxy' do
            r = Requests.get(@url, proxies: @proxies)
            sleep(1)  # wait for log to be written to file
            new_log_entries = @proxy_log.readlines
            expect(r.status_code).to eq(200)
            msg = /GET #{@url} #{proxy_user}/
            expect(new_log_entries.any? { |x| x =~ msg }).to eq(true)
        end

        it "through proxy, wrong credentials returns 407 response" do
            r = Requests.get(@url, proxies: @proxies_no_creds)
            sleep(1)  # wait for log to be written to file
            new_log_entries = @proxy_log.readlines
            expect(r.status_code).to eq(407)
            msg = /TCP_DENIED\/407 .* GET #{@url}/
            expect(new_log_entries.any? { |x| x =~ msg }).to eq(true)
        end
    end

    context "HTTPS" do
        before(:each) do
            @url = HTTPBIN_URLS['HTTPS'] + '/anything'
        end

        it 'through proxy' do
            r = Requests.get(@url, proxies: @proxies)
            sleep(1)  # wait for log to be written to file
            new_log_entries = @proxy_log.readlines
            expect(r.status_code).to eq(200)
            msg = /CONNECT #{URI(@url).host}/
            expect(new_log_entries.any? { |x| x =~ msg }).to eq(true)
        end

        it "through proxy, wrong credentials raises ProxyAuthError" do
            expect {
                Requests.get(@url, proxies: @proxies_no_creds)
            }.to raise_error(Requests::ProxyAuthError)
            sleep(1)  # wait for log to be written to file
            new_log_entries = @proxy_log.readlines
            msg = /TCP_DENIED\/407 .* CONNECT #{URI(@url).host}/
            expect(new_log_entries.any? { |x| x =~ msg }).to eq(true)
        end
    end
end
