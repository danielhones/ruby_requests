require 'rspec'
require_relative '../spec_helper'


RSpec.describe Requests::Utils do
    describe 'InsensitiveDict' do
        let(:sample_values) {
            {'KEY A' => 'value a',
             'Key B' => 'Value B',
             'test key' => 'test value'}
        }

        before(:each) do
            @dict = Requests::Utils::InsensitiveDict.new
        end

        it "stores values" do
            @dict['key'] = 'value'
            expect(@dict['key']).to eq('value')
        end

        it "keys are case-insensitive" do
            @dict['Some-Key'] = "Some value"
            expect(@dict['Some-Key']).to eq("Some value")
            expect(@dict['some-key']).to eq("Some value")
            expect(@dict['SOME-KEY']).to eq("Some value")
        end

        it "keys must be strings" do
            expect {
                @dict[:key] = 'value'
            }.to raise_error(TypeError)
        end

        it "values get overwritten if different case" do
            @dict['initial'] = 'first value'
            @dict['INITIAL'] = 'second value'
            expect(@dict['initial']).to eq('second value')
        end

        it "merges another dict" do
            orig_values = {
                'Content-Type' => 'application/json',
                'Accept-Encoding' => 'identity',
                'Accept' => '*/*',
            }
            dict = Requests::Utils::InsensitiveDict.new(orig_values)
            merged = dict.merge({'New-Header' => 'example',
                                 'Content-Type' => 'text/plain'})
            expect(merged['Content-Type']).to eq('text/plain')
            expect(merged['Accept-Encoding']).to eq('identity')
            expect(merged['Accept']).to eq('*/*')
            expect(merged['New-Header']).to eq('example')
            # make sure not mutated:
            expect(dict).to eq(orig_values)
        end

        it "merges another dict and mutates" do
            orig_values = {
                'Content-Type' => 'application/json',
                'Accept-Encoding' => 'identity',
                'Accept' => '*/*',
            }
            dict = Requests::Utils::InsensitiveDict.new(orig_values)
            dict.merge!({'New-Header' => 'example',
                         'Content-Type' => 'text/plain'})
            expect(dict['Content-Type']).to eq('text/plain')
            expect(dict['Accept-Encoding']).to eq('identity')
            expect(dict['Accept']).to eq('*/*')
            expect(dict['New-Header']).to eq('example')
        end

        describe "==" do
            before(:each) do
                @orig_values = sample_values
                @dict.merge!(@orig_values)
            end

            it "equal to InsensitiveDict" do
                other_dict = Requests::Utils::InsensitiveDict.new(@orig_values)
                expect(@dict).to eq(other_dict)
            end

            it "not equal to InsensitiveDict" do
                other_dict = Requests::Utils::InsensitiveDict
                             .new({'some other' => 'values'})
                expect(@dict).not_to eq(other_dict)
            end

            it "equal to hash" do
                expect(@dict).to eq(@orig_values)
            end

            it "not equal to hash" do
                other_hash = {'some other' => 'values'}
                expect(@dict).not_to eq(other_hash)
            end

            it "not equal to hash with non-string keys" do
                other_hash = {:symbol_key => 'value'}
                expect(@dict).not_to eq(other_hash)
            end
        end

        it "delete deletes keys case-insensitive" do
            @dict.merge!(sample_values)
            @dict.delete('key a')
            @dict.delete('key b')
            expect(@dict).to eq({'test key' => 'test value'})
        end
    end
end
