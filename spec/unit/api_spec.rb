##
# This file contains the unit tests for the top level functions on the
# Requests module, defined in lib/requests.rb.

require 'rspec'
require 'securerandom'
require_relative '../spec_helper'


RSpec.describe Requests do
    describe "logging" do
        before(:each) do
            @log = StringIO.new
            @logger = Requests::Logger.new(@log)
            @logger.level = 'debug'
            Requests.logger = @logger
            # A unique string we can check for in the log:
            @msg = SecureRandom.uuid
        end

        it "logs to user-provided logger" do
            Requests.logger.warn(@msg)
            @log.seek(0)
            expect(@log.read).to match(@msg)
        end

        it "disable_logging prevents logging" do
            Requests.disable_logging
            Requests.logger.warn(@msg)
            @log.seek(0)
            expect(@log.read).to eq('')
        end

        it "enable_logging re-enables logging" do
            Requests.disable_logging
            Requests.enable_logging
            Requests.logger.warn(@msg)
            @log.seek(0)
            expect(@log.read).to match(@msg)
        end
    end
end
