#!/bin/bash

this_dir="$(dirname $0)"
docker_compose_file="$this_dir/docker-compose.yml"

docker-compose -f "$docker_compose_file" up --exit-code-from ruby
