#!/bin/bash
#
# This is the script used as the entrypoint for running proxy
# tests from within a docker container as part of docker-compose

cd $REPO_ROOT_DIR
gem install -g Gemfile
rspec spec/
result=$?

echo "ENTRYPOINT: Rspec returned, exiting with code $result"
exit $result
