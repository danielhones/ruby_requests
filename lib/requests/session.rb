module Requests
    class Session
        include HttpMethods

        attr_accessor :headers, :cookies, :auth, :proxies, :params, :verify,
                      :cert, :stream, :max_redirects, :trust_env
        attr_reader :logger
        private :logger

        DEFAULT_REDIRECT_LIMIT = 30

        def initialize
            @logger = Requests.logger
            @headers = Utils.default_headers
            @cookies = CookieJar.new
            @proxies = {}
            @params = {}
            @verify = true
            @stream = false
            @trust_env = true
            @max_redirects = DEFAULT_REDIRECT_LIMIT
        end

        def request(method, url, params: {}, data: nil, headers: {},
                    cookies: {}, files: nil, proxies: {}, stream: false,
                    auth: nil, timeout: nil, json: nil, allow_redirects: true,
                    verify: true)
            headers = @headers.merge(headers)
            req = Request.new(method.upcase,
                              url,
                              headers: headers,
                              files: files,
                              params: params,
                              data: data,
                              json: json,
                              cookies: cookies,
                              auth: auth)

            prepare_request(req)
            settings = merge_environment_settings(
                req.url, proxies, stream, verify, cert
            )
            send_kwargs = {
                :timeout => timeout,
                :allow_redirects => allow_redirects,
            }.merge(settings)
            send_request(req, **send_kwargs)
        end

        def prepare_request(request)
            merged_cookies = CookieJar.new
            merged_cookies.merge!(@cookies)
            merged_cookies.merge!(request.cookies)

            request.headers = merge_setting(request.headers, @headers,
                                            Utils::InsensitiveDict)
            request.params = merge_setting(request.params, @params)
            request.auth = merge_setting(request.auth, @auth)
            request.cookies = merged_cookies
            request.prepare
            request
        end

        def send_request(request, **kwargs)
            kwargs[:stream] = kwargs.fetch(:stream, @stream)
            kwargs[:verify] = kwargs.fetch(:verify, @verify)
            kwargs[:cert] = kwargs.fetch(:cert, @cert)
            kwargs[:proxies] = kwargs.fetch(:proxies, @proxies)

            ## Use the following line once implementing redirects:
            # allow_redirects = kwargs.delete(:allow_redirects) || true
            kwargs.delete(:allow_redirects)

            response = adapter.send_request(request, **kwargs)
            @cookies.merge!(response.cookies)
            response
        end

        def merge_environment_settings(_url, proxies, stream, verify, cert)
            # TODO: Implement actually using environment settings
            proxies = merge_setting(proxies, @proxies)
            stream = merge_setting(stream, @stream)
            verify = merge_setting(verify, @verify)
            cert = merge_setting(cert, @cert)

            {verify: verify,
             proxies: proxies,
             stream: stream,
             cert: cert}
        end

        ##
        # Determine appropriate setting for a given request, taking into account
        # the explicit setting on that request, and the setting in the session.
        # If a is a Hash or InsensitiveDict they will be merged together using
        # `dict_class`

        def merge_setting(request_setting, session_setting, dict_class=Hash)
            return request_setting if session_setting.nil?
            return session_setting if request_setting.nil?

            map_types = [Hash, Requests::Utils::InsensitiveDict]
            if !(map_types.include?(request_setting.class) &&
                 map_types.include?(session_setting.class))
                return request_setting
            end

            merged_setting = dict_class.new.merge(session_setting)
            merged_setting.merge!(request_setting)

            nil_keys = merged_setting.select { |_k, v| v.nil? }.map(&:first)
            nil_keys.each { |key| merged_setting.delete(key) }
            merged_setting
        end

        def adapter
            HTTPAdapter.new
        end
    end
end
