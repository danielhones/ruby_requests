require 'http-cookie'


module Requests
    class Cookie < HTTP::Cookie
    end

    class CookieJar < HTTP::CookieJar
        ##
        # Return Hash of cookies from this jar for a given uri.
        # If provided no argument, it returns a Hash of all cookie
        # key/value pairs in the jar, but this can lose information
        # if the same cookie key was stored for different URIs.
        #
        # @param uri [URI String] optional

        def to_hash(uri=nil)
            hash = {}
            cookies(uri).each do |c|
                hash[c.name] = c.value
            end
            hash
        end

        ##
        # Given another CookieJar, copy the cookies with this one

        def merge!(jar)
            jar.each do |cookie|
                add(cookie.dup)
            end
            self
        end

        ##
        # Given an array of Set-Cookie header values, parse each
        # and add them to this cookie jar.
        #
        # @param header [Array]
        # @param uri [URI String] URI these cookies belong to

        def set_cookies_from_header(header, uri)
            header.each do |value|
                parse(value, uri)
            end
            self
        end

        def self.from_hash(hash, domain)
            jar = new
            hash.each do |key, value|
                cookie = Cookie.new(key, value, domain: domain,
                                    path: '/', for_domain: true)
                jar.add(cookie)
            end
            jar
        end
    end

    module CookiesMixin
        ##
        # The following methods depend on the instance variable
        # @uri being defined

        def cookies=(cookies)
            if cookies.is_a?(Hash)
                domain = @uri.hostname.downcase
                @cookies = CookieJar.from_hash(cookies, domain)
            else
                @cookies = cookies
            end
        end

        def cookies
            @cookies ||= CookieJar.new
            @cookies
        end

        def cookies_hash
            @cookies.to_hash(@uri)
        end

        def cookie_header
            Cookie.cookie_value(@cookies.cookies(@uri))
        end

        ##
        # Given an array of Set-Cookie header values, add them
        # to @cookies

        def set_cookies_from_header(header, uri)
            return if header.nil? || header.empty?
            @cookies ||= CookieJar.new
            @cookies.set_cookies_from_header(header, uri)
            @cookies
        end
    end
end
