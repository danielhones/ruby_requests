require 'json'
require 'zlib'


module Requests
    class Response
        include CookiesMixin
        include Utils::HeadersMixin

        attr_accessor :status_code, :body, :request, :reason, :cookie_jar,
                      :uri, :raw

        attr_reader :logger, :raw_headers
        private :logger

        def initialize(status_code:, reason:, headers:, body:, uri:,
                       request:, raw: nil)
            @logger = Requests.logger
            @status_code = status_code
            @reason = reason
            @raw_headers = Requests::Utils::InsensitiveDict.new(headers)
            self.headers = headers
            @body = body ? decode_content(body) : ''
            @uri = URI(uri)
            @request = request
            @raw = raw
            set_cookies_from_header(@raw_headers['set-cookie'], @request.uri)
            fix_string_encoding
        end

        def inspect
            "#<Requests::Response #{@status_code} #{@reason}>"
        end

        def url
            uri.to_s
        end

        def text
            body
        end

        def json
            JSON.parse(body)
        end

        def decode_content(body)
            if content_encoding == 'identity'
                body
            elsif content_encoding == 'gzip'
                str_io = StringIO.new(body)
                gz = Zlib::GzipReader.new(str_io)
                gz.read
            elsif content_encoding == 'deflate'
                Zlib::Inflate.inflate(body)
            end
        end

        def content_encoding
            @headers['content-encoding'] || 'identity'
        end

        def fix_string_encoding
            charset = charset_from_content_type

            if charset
                begin
                    encoding = Encoding.find(charset)
                    logger.debug("Forcing encoding for response body " \
                                 "using charset: #{charset.inspect}")
                    @body.force_encoding(encoding)
                rescue ArgumentError
                    logger.error("No such encoding: #{charset.inspect}")
                end
            end
        end

        def charset_from_content_type
            Requests::Utils.charset_from_content_type(headers['Content-Type'])
        end

        def self.from_net_http_response(resp, request)
            body = resp.body rescue nil
            self.new(status_code: resp.code.to_i,
                     reason: resp.message,
                     headers: resp.to_hash,
                     body: body,
                     uri: request.uri,
                     request: request,
                     raw: resp)
        end
    end
end
