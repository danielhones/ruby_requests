module Requests
    module HttpMethods
        def get(url, params: {}, **kwargs)
            kwargs[:allow_redirects] = kwargs.fetch(:allow_redirects, true)
            request('get', url, params: params, **kwargs)
        end

        def post(url, params: {}, data: nil, json: nil, **kwargs)
            request('post', url, params: params, data: data,
                    json: json, **kwargs)
        end

        def put(url, params: {}, data: nil, json: nil, **kwargs)
            request('put', url, params: params, data: data,
                    json: json, **kwargs)
        end

        def delete(url, **kwargs)
            request('delete', url, **kwargs)
        end

        def head(url, **kwargs)
            kwargs[:allow_redirects] = kwargs.fetch(:allow_redirects, false)
            request('head', url, **kwargs)
        end

        def options(url, **kwargs)
            kwargs[:allow_redirects] = kwargs.fetch(:allow_redirects, true)
            request('options', url, **kwargs)
        end

        def patch(url, **kwargs)
            request('patch', url, **kwargs)
        end
    end
end
