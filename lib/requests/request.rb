require 'json'
require 'net/http'
require 'uri'


module Requests
    class Request
        include CookiesMixin
        include Utils::HeadersMixin

        attr_accessor :method, :url, :uri, :files, :data, :params, :auth,
                      :json, :body
        attr_reader :logger, :raw
        private :logger

        def initialize(method, url, headers: {}, files: nil, data: nil,
                       params: {}, auth: nil, cookies: nil, json: nil)
            @logger = Requests.logger
            @method = method
            @url = url
            @uri = URI(@url)
            @headers = headers

            @files = files
            if @files
                raise(NotImplementedError,
                      "files keyword param is not yet implemented")
            end

            @data = data
            @params = params
            @auth = auth
            self.cookies = cookies
            @json = json
        end

        def inspect
            "#<Requests::Request #{@method}>"
        end

        def prepare
            if @params && !@params.empty?
                @uri.query = URI.encode_www_form(@params)
            end
            set_body_and_content_type
            @raw = build_request
            set_request_auth(@raw)
            self.headers = @raw.to_hash
        end

        def set_body_and_content_type
            if @json
                @headers['Content-Type'] = 'application/json'
                @body = @json.to_json
            elsif @data.is_a?(String)
                @headers['Content-Type'] ||= 'text/plain'
                @body = @data
            elsif @data.is_a?(Array)
                @headers['Content-Type'] ||= 'application/x-www-form-urlencoded'
                @body = URI.encode_www_form(Hash[@data])
            elsif @data.is_a?(Hash)
                @headers['Content-Type'] ||= 'application/x-www-form-urlencoded'
                @body = URI.encode_www_form(@data)
            end
        end

        def build_request
            req = http_method_class.new(uri.request_uri)
            req.body = body
            @headers['cookie'] = cookie_header if !cookies.empty?

            @headers.each do |key, value|
                req[key] = value
            end
            req
        end

        ##
        # @param request: Net::HTTPRequest object to set authentication on
        #                 Currently only basic auth is supported

        def set_request_auth(request)
            if @auth && @auth.is_a?(Array)
                user, pass = @auth
                request.basic_auth(user, pass)
            end
            request
        end

        def http_method_class
            Requests::Utils.http_method_class(@method)
        end
    end
end
