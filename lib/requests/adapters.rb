require 'net/http'
require 'openssl'


##
# Initial implementation here will be very simple and lack features like
# connection pooling.

module Requests
    class BaseAdapter
        attr_reader :logger
        private :logger

        def initialize
            @logger = Requests.logger
        end

        # :nocov:
        # rubocop:disable Lint/UnusedMethodArgument
        def send_request(_request, stream: false, timeout: nil, verify: true,
                         cert: nil, proxies: {})
            raise(NotImplementedError, "send_request must be implemented by " \
                                       "subclasses of #{self.class.name}")
        end
        # rubocop:enable Lint/UnusedMethodArgument

        def close
            raise(NotImplementedError, "close must be implemented by " \
                                       "subclasses of #{self.class.name}")
        end
        # :nocov:

        def determine_proxy(uri, proxies)
            scheme_host = "#{uri.scheme}://#{uri.host}"
            proxy = proxies[scheme_host] || proxies[uri.scheme]
            proxy ? URI(proxy) : nil
        end
    end

    class HTTPAdapter < BaseAdapter
        def send_request(request, stream: false, timeout: nil, verify: true,
                         cert: nil, proxies: {})
            logger.info("Request URI: #{request.uri.inspect}")

            if stream
                raise(NotImplementedError,
                      "streaming response body is not yet implemented")
            end

            proxy = determine_proxy(request.uri, proxies)
            http = create_http(request.uri, proxy, verify, cert)

            if timeout.is_a?(Array)
                http.open_timeout, http.read_timeout = timeout
            else
                http.open_timeout = timeout
                http.read_timeout = timeout
            end

            # :nocov:
            if ENV['REQUESTS_HTTP_DEBUG'] == 'true'
                logger.debug("Setting Net::HTTP debug output to stdout")
                http.set_debug_output($stdout)
            end
            # :nocov:

            begin
                resp = http.start do
                    http.request(request.raw)
                end
            rescue Net::OpenTimeout => e
                raise ConnectionTimeout.new('Connection timed out.',
                                            from_error: e, request: request)
            rescue Net::ReadTimeout => e
                raise ReadTimeout.new('Read timed out.',
                                      from_error: e, request: request)
            rescue Net::HTTPServerException => e
                if e.message == '407 "Proxy Authentication Required"'
                    resp = Response.from_net_http_response(e.response,
                                                           request)
                    raise ProxyAuthError.new(e.message,
                                             from_error: e,
                                             request: request,
                                             response: resp)
                else
                    raise e
                end
            end

            body_size = resp.body ? resp.body.bytesize : 0
            logger.debug("Response body: #{body_size} bytes")

            # TODO: handle redirects

            response = Response.from_net_http_response(resp, request)
            logger.info("Response: #{response.inspect}")
            response
        end

        def create_http(uri, proxy, verify, _cert)
            if proxy
                logger.info("Using proxy: #{proxy.scheme}://#{proxy.host}:" \
                            "#{proxy.port}")
                http = Net::HTTP.new(uri.host, uri.port, proxy.host, proxy.port)
                http.proxy_user = proxy.user
                http.proxy_pass = proxy.password
            else
                http = Net::HTTP.new(uri.host, uri.port, nil, nil)
            end

            http.use_ssl = (uri.scheme == 'https')

            if verify == false
                http.verify_mode = OpenSSL::SSL::VERIFY_NONE
            elsif verify.is_a?(String)
                http.verify_mode = OpenSSL::SSL::VERIFY_PEER
                http.ca_file = verify
            else
                http.verify_mode = OpenSSL::SSL::VERIFY_PEER
            end
            http
        end
    end
end
