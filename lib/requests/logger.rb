require 'logger'


module Requests
    class LogFormatter
        FORMAT = "%<severity>s [%<time>s] %<msg>s\n".freeze

        def call(severity, time, _, msg)
            FORMAT % {:severity => severity.ljust(5),
                      :time => format_time(time),
                      :msg => msg}
        end

        def format_time(time)
            time.iso8601
        end
    end

    class Logger < ::Logger
        DEFAULT_LOG_DEV = STDOUT
        DEFAULT_FORMATTER = LogFormatter.new

        attr_accessor :enabled

        def initialize(logdev, shift_age=0, shift_size=1048576, level: DEBUG,
                       enabled: true)
            super(logdev, shift_age, shift_size)
            @enabled = enabled
            self.formatter = formatter || DEFAULT_FORMATTER
            self.level = level
        end

        def add(severity, message=nil, progname=nil)
            super(severity, message, progname) if @enabled
        end
    end
end
