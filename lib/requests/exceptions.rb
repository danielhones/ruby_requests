require 'net/http'


module Requests
    class RequestException < RuntimeError
        attr_reader :request, :response, :message, :from_error

        def initialize(msg, from_error: nil, request: nil, response: nil)
            super(msg)
            @message = msg
            @from_error = from_error
            @request = request
            @response = response
        end

        def inspect
            "#<#{self.class.name} #{@message}>"
        end
    end

    class TimeoutError < RequestException
    end

    class ConnectionTimeout < TimeoutError
    end

    class ReadTimeout < TimeoutError
    end

    class ProxyAuthError < RequestException
    end
end
