require 'forwardable'


module Requests
    module Utils
        ##
        # Dictionary that stores keys as case-insensitive strings.
        # Only Strings can be used as keys in this structure; attempts
        # to use anything else as a key will raise a TypeError.

        class InsensitiveDict
            attr_accessor :store, :dict

            extend Forwardable
            include Enumerable

            def_delegators :@dict, :each

            def initialize(init_dict={})
                @store = {}
                @dict = {}
                init_dict.each do |k, v|
                    self[k] = v
                end
            end

            ##
            # Can compare to another InsensitiveDict or to  a regular Hash.

            def ==(compare)
                compare.each do |k, v|
                    return false if !k.is_a?(String) || self[k.downcase] != v
                end
                true
            end

            def []=(k, v)
                check_key_type(k)
                @store[k.downcase] = [k, v]
                @dict[k.downcase] = v
            end

            def [](k)
                check_key_type(k)
                @dict[k.downcase]
            end

            def merge!(hash)
                hash.each do |k, v|
                    self[k] = v
                end
                self
            end

            def merge(new)
                hash = to_h
                new.each do |k, v|
                    hash[k] = v
                end
                hash
            end

            def to_h
                hash = {}
                @store.each_value do |i|
                    orig, value = i
                    hash[orig] = value
                end
                hash
            end

            def inspect
                to_h.inspect
            end

            def to_s
                to_h.to_s
            end

            def delete(key)
                if key.is_a?(String)
                    key = key.downcase
                    @store.delete(key)
                    @dict.delete(key)
                end
            end

            private def check_key_type(key)
                if !key.is_a?(String)
                    err = "Key for #{self.class.name} object " \
                          "must be String, not #{key.class.name}"
                    raise(TypeError, err)
                end
            end
        end

        def self.default_user_agent
            "ruby-requests/#{Requests::VERSION}"
        end

        def self.default_accept_encoding
            'gzip, deflate'
        end

        def self.default_headers
            headers = {
                'User-Agent' => default_user_agent,
                'Accept-Encoding' => default_accept_encoding,
                'Accept' => '*/*',
                'Connection' => 'keep-alive',
            }
            InsensitiveDict.new(headers)
        end

        module HeadersMixin
            ##
            # Set @headers to InsensitiveDict keyed by String with Strings as
            # values. When passed the result of Net::HTTPResponse.to_hash,
            # which is a Hash keyed by String with Arrays as values, it will
            # join the elements in the Array by ', ' as the header value.
            # 'Set-Cookie' is handled specially because a comma is valid in a
            # cookie definition, so it is joined by '; ' instead

            def headers=(hash)
                @headers = Utils::InsensitiveDict.new
                hash.each do |k, v|
                    delimiter = ', '
                    @headers[k] = v.is_a?(Array) ? v.join(delimiter) : v
                end
            end

            def headers
                @headers
            end
        end

        ##
        # Given a string indicating an HTTP method, return the
        # corresponding Net::HTTP class

        def self.http_method_class(method)
            {
                'GET' => Net::HTTP::Get,
                'POST' => Net::HTTP::Post,
                'PUT' => Net::HTTP::Put,
                'DELETE' => Net::HTTP::Delete,
                'HEAD' => Net::HTTP::Head,
                'OPTIONS' => Net::HTTP::Options,
                'PATCH' => Net::HTTP::Patch,
            }[method.upcase]
        end

        def self.charset_from_content_type(header)
            charset = nil
            if header
                match = header.match('charset=([^\s;]+)')
                charset = match.captures[0] if match
            end
            charset
        end
    end
end
