dir = File.expand_path(__dir__)

require File.join(dir, 'requests/version')
require File.join(dir, 'requests/exceptions')
require File.join(dir, 'requests/logger')
require File.join(dir, 'requests/utils')
require File.join(dir, 'requests/cookies')
require File.join(dir, 'requests/http_methods')
require File.join(dir, 'requests/request')
require File.join(dir, 'requests/response')
require File.join(dir, 'requests/adapters')
require File.join(dir, 'requests/session')


module Requests
    ##
    # Constructs and sends a :class:`Request <Request>`.
    #
    # :param method: method for the new :class:`Request` object.
    # :param url: URL for the new :class:`Request` object.
    # :param params: (optional) Hash be sent in the query string
    # :param data: (optional) Hash, list of tuples ``[[key, value]]`, string,
    #     or file-like object to send in the body.
    # :param json: (optional) json data to send in the body. Using this param
    #     will ignore any +data+ keyword param
    # :param headers: (optional) Hash of HTTP Headers to send
    # :param cookies: (optional) Hash or HTTP::CookieJar object to send
    # :param auth: (optional) Auth tuple to enable basic auth or
    # :param timeout: (optional) How many seconds to wait for the server to send
    #     data before giving up.  Float or , or an array of Floats indicating
    #     (connect timeout, read timeout)
    # :param allow_redirects: (optional) Boolean. Enable/disable redirection.
    #     Defaults to +true+.
    #
    # :return: :class:`Response <Response>` object
    # :rtype: Requests::Response
    #
    # Not implemented yet:
    # :param verify: (optional) Either a boolean, in which case it controls
    #     whether we verify the server's TLS certificate, or a string, in which
    #     case it must be a path to a CA bundle to use. Defaults to +true+
    # :param files: (optional) Hash of +'name': file-like-objects+
    # :param proxies: (optional) Dictionary mapping protocol to the URL of the
    #     proxy.
    # :param stream: (optional) if ``False``, the response content will be
    #     immediately downloaded.
    # :param cert: (optional) if String, path to ssl client cert file (.pem).
    #     If Tuple, ('cert', 'key') pair.

    extend HttpMethods

    def self.request(method, url, **kwargs)
        session = Session.new
        session.request(method, url, **kwargs)
    end

    ##
    # Logging related stuff:

    DEFAULT_LOG_DEV = $stdout
    DEFAULT_LOGGING_ENABLED = false

    def self.default_logger
        # :nocov:
        Requests::Logger.new(DEFAULT_LOG_DEV, level: 'info',
                             enabled: DEFAULT_LOGGING_ENABLED)
        # :nocov:
    end

    def self.logger
        @logger ||= default_logger
    end

    def self.logger=(new_logger)
        @logger = new_logger
    end

    def self.enable_logging
        logger.enabled = true if logger.respond_to?(:'enabled=')
    end

    def self.disable_logging
        logger.enabled = false if logger.respond_to?(:enabled)
    end

    ##
    # Shortcut to enable debug logging for development

    def self.enable_dev_mode
        # :nocov:
        enable_logging
        logger.level = 'debug'
        # :nocov:
    end
end
