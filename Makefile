RUBY_VERSIONS := 2.3.0 2.4.0 2.5.0

.PHONY: test-all-versions
test-all-versions:
	@$(foreach version, $(RUBY_VERSIONS), \
	echo "Running tests in Ruby $(version)"; \
	rvm $(version) do rspec; \
	)

.PHONY: setup-rvm-versions
setup-rvm-versions:
	@$(foreach version, $(RUBY_VERSIONS), \
	echo "Setting up $(version)"; \
	rvm install $(version); \
	rvm $(version) do gem install -g Gemfile; \
	echo ""; \
	)
