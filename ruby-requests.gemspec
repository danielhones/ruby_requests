require File.expand_path('lib/requests/version', __dir__)


Gem::Specification.new do |s|
    s.name = 'ruby-requests'
    s.version = Requests::VERSION
    s.authors = ['Daniel Hones']
    s.description = "A library for HTTP requests that aims to provide " \
                    "the same API as the Requests library in Python"
    s.summary = 'A Ruby version of the Requests library for Python'

    s.license = 'MIT'
    s.homepage = 'https://gitlab.com/danielhones/ruby_requests'

    s.files = `git ls-files`.split("\n")
    s.test_files = `git ls-files -- {spec}/**/*`.split("\n")
    s.required_ruby_version = ">= 2.3.0"

    s.add_dependency('http-cookie', '~> 1.0')
    s.add_development_dependency('rspec', '~> 3.7')
    s.add_development_dependency('rubocop', '~> 0.54.0')
    s.add_development_dependency('simplecov', '~> 0.15')
end
